import { isJson } from "./utils";

/**
 * Store the value at the specified key. Automaticly stringify Javascript object
 *
 * @param {string} key The key of the cookie
 * @param {*} value The value stored in the cookie, can be string, or a Javascript object(array/object)
 * @param {Number} [day=7] Number of day before the expire of the cookie
 * @param {string} [path="/"] Path of the cookie
 */
export const setItem = (key, value, days = 7, path = "/") => {
  const stringVal = JSON.stringify(value);
  var d = new Date();
  d.setTime(d.getTime() + days * 24 * 60 * 60 * 1000);
  document.cookie = `${key}=${encodeURIComponent(stringVal)}; expires=${
    d.toUTCString
  }; path=${path}
  `;
};

/**
 * Get the cookie at the key passed in parameter. If it's a JSON format, automaticly parse it
 * @returns The item if found, or -1
 * @param {string} key The key of the cookie
 */
export const getItem = (key) => {
  const item = document.cookie.split("; ").reduce((curr, next) => {
    const parts = next.split("=");
    return parts[0] === key ? decodeURIComponent(parts[1]) : curr;
  }, -1);
  return isJson(item) ? JSON.parse(item) : item;
};

/**
 * Expire and clear the cookie if found
 * @returns true if the item is found, else return false
 * @param {string} key
 * @param {string} [path="/"]
 */
export const removeItem = (key, path = "/") => {
  const item = document.cookie.split("; ").reduce((curr, next) => {
    const parts = next.split("=");
    return parts[0] === key ? decodeURIComponent(parts[1]) : curr;
  }, -1);
  if (item !== -1) {
    setItem(key, "", -1, path);
    return true;
  } else {
    return false;
  }
};
