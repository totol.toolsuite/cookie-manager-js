# Cookie management js

## Package in order to simplify cookies management in javascript
- Contains methods to simplify cookies management in javascript. 
- Contains methods to add, get, delete, and update your cookies.

## Requirement 
- No requirement

## Usage

### Installation
``` npm i instagram-graph-fetcher-js ```

### Example
``` 
import { getItem, removeItem, setItem } from "cookie-management-js";

// Will store a cookie at the key "testKey", with the value "testValue", that will expires in 3 days, at the path "/testPath"
setItem("testKey", "testValue", 3, "/testPath");

// >> "testValue"
console.log(getItem("testKey"));

// The cookie will be cleared, and expired
deleteItem("testKey", "/testPath");

// >> ""
console.log(getItem("testKey"));
```

## Methods list

### setItem
#### Description
-  Store the value at the specified key. Automaticly stringify Javascript object

#### Usage
``` 
import { setItem } from "cookie-management-js"; 
setItem("key1", "a string expiring in 3 days at the path /path", 3, "/path");
setItem("key2", {"name": "an object expiring in 7 days at the path /"});
setItem("key3", [1, 2, 3]);
```
#### Parameters
 - @param {string} key The key of the cookie
 - @param {*} value The value stored in the cookie, can be string, or a Javascript object(array/object)
 - @param {Number} [day=7] Number of day before the expire of the cookie
 - @param {string} [path="/"] Path of the cookie
#### Response
void

### getItem
#### Description
- Get the cookie at the key passed in parameter. If it's a JSON format, automaticly parse it

#### Usage
``` 
// Use the values stored in the example of the setItem method
import { getItem } from "cookie-management-js"; 

// >> "a string expiring in 3 days at the path /path"
console.log("key1")

// >> {name: "an object expiring in 7 days at the path /"}
console.log("key2")

// >> [1, 2, 3]
console.log("key3")

// >> -1
console.log("key4")
```
#### Parameters
 - @param {string} key The key of the cookie

#### Response
The content of the cookie, JSON parsed if possible.
If no content is found at this key, return -1

### removeItem
#### Description
- Expire and clear the cookie if found

#### Usage
``` 
// Use the values stored in the example of the setItem method
import { removeItem, getItem } from "cookie-management-js"; 

// >> true
console.log(removeItem("key1"));
// After remove
// >> ""
console.log(getItem("key1"));

// >> false
console.log(removeItem("key4"))
```
#### Parameters
 - @param {string} key The key of the cookie
 - @param {string} path The path of the cookie

#### Response
If the cookie is deleted, returns true.
If the key or path is wrong, returns false